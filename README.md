
## Upload a large file using Node.js

For correct work you will need node.js v6.10.0 and mongodb v3.*

#### run server:
```
$ npm start
```

#### run server in development mode:
```
$ npm run serve
```


#### run tests:
```
$ npm test
```