
const pm2 = require('pm2');


pm2.connect(() => {
  pm2.start({
    script: './server/server.js',
    name: 'large-file-uploader',
    instances: 1,
    args: [],
    max_memory_restart: `${process.env.WEB_MEMORY || 512}M`,
    env: {
      NODE_CONFIG_DIR: './server/config'
    }
  }, err => {
    if (err) {
      return console.error('Error while launching applications', err.stack || err);
    }

    console.info('PM2 and application has been succesfully started');

    // Display logs in standard output
    pm2.launchBus((err, bus) => {
      console.info('[PM2] Log streaming started');

      bus.on('log:out', packet => {
        const data = packet.data && packet.data.type === 'Buffer' ? new Buffer(packet.data.data).toString() : packet.data; // eslint-disable-line max-len
        console.info('[App:%s] %s', packet.process.name, data);
      });

      bus.on('log:err', packet => {
        const data = packet.data && packet.data.type === 'Buffer' ? new Buffer(packet.data.data).toString() : packet.data; // eslint-disable-line max-len
        console.error('[App:%s][Err] %s', packet.process.name, data);
      });
    });
  });
});
