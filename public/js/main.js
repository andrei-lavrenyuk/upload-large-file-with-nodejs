
$(document).ready(function() {
  var socket = io.connect('/upload');

  var $result = $('#result');
  var $progress = $('#progress-bar');  
  var $exception = $('#exception');  
  var $fileEl = $('#file');  
  
  $('#upload').click(function() {
    if (!$fileEl[0].files[0]) {
      return;
    }

    // clear all when start new uploading
    $progress.text('');
    $exception.text('');
    $result.text('');
    
    var file = $fileEl[0].files[0];
    var stream = ss.createStream();
    
    // upload a file to the server.
    ss(socket).emit('file', stream, { size: file.size });

    var blobStream = ss.createBlobReadStream(file);
    var size = 0;

    // display progress
    ss(socket).on('data', function(str) {
      size += str.length;
      
      $progress.text(Math.floor(size / file.size * 100) + '%');
      $result.append(str);
    });

    // show error message
    ss(socket).on('exception', function(exc) {
      $exception.text(exc.errorMessage);
    });

    // clear file input
    ss(socket).on('end', function() {
      $fileEl.val('');
    });

    blobStream.pipe(stream);
  });
});