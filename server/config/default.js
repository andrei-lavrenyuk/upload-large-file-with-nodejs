
module.exports = {
  app: {
    port: process.env.PORT || 3000
  },
  mongo: {
    hosts: [
      process.env.MONGOLAB_URI ||
      'mongodb://localhost:27017/large-file-uploader'
    ],
    options: {
      server: {
        socketOptions: {
          keepAlive: 1
        }
      }
    }
  }
};
