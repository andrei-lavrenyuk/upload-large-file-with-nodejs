
module.exports = {
  mongo: {
    hosts: [
      'mongodb://localhost:27017/large-file-uploader-test'
    ],
    options: {
      server: {
        socketOptions: {
          keepAlive: 1
        }
      }
    }
  }
};
