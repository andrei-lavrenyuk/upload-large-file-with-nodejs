
const transform = require('stream-transform');


module.exports = {
  arrToCSVSting(arr) {
    return `${arr.join(',')}\n`;
  },

  transformStream() {
    let firstLine;
    return transform((record, callback) => {
      if (!firstLine) {
        firstLine = record;
        return callback(null, { record });
      }
      
      const data = {
        name: record[0],
        surname: record[1],
        email: record[2]
      };

      callback(null, { record, data });
    }, { parallel: 1 });
  }
};
