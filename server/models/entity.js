
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  surname: { type: String, required: true },
  email: { type: String, required: true }
}, {
  collection: 'entities'
});

module.exports = mongoose.model('Entity', schema);
