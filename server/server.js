
const config = require('config');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const ss = require('socket.io-stream');
const parse = require('csv-parse');
const mongoose = require('mongoose');

const utils = require('./lib/utils');
const Entity = require('./models/entity');


mongoose.connect(config.mongo.hosts.join(','), config.mongo.options);
mongoose.connection.on('error', console.log);


const app = express();
const server = http.createServer(app);
const io = socketIO.listen(server);


app.use(express.static(`${__dirname}/../public`));
app.get('/', express.static(`${__dirname}/../public/index.html`));


io.on('connection', () => {
  console.log('Connected succesfully to the socket ...');
});

io.of('/upload').on('connection', socket => {
  ss(socket).on('file', stream => {
    const parser = parse();
    const transformer = utils.transformStream();

    stream.pipe(parser).pipe(transformer);

    transformer.on('data', result => {
      if (result.record.length <= 1) { // case with empty line
        return null;
      } else if (!result.data) { // case with first line
        ss(socket).emit('data', utils.arrToCSVSting(result.record));
      } else {
        new Entity(result.data).save(err => {
          if (err) {
            return ss(socket).emit('exception', { errorMessage: err.message || 'An error has occurred.' });
          }
          ss(socket).emit('data', utils.arrToCSVSting(result.record));
        });
      }
    });

    transformer.on('error', err => {
      ss(socket).emit('exception', { errorMessage: err.message || 'An error has occurred.' });
    });

    transformer.on('end', () => {
      ss(socket).emit('end');
    });
  });
});

server.listen(config.app.port, () => {
  console.info(`Server listening at ${config.app.port} port`);
});
