
const fs = require('fs');
const parse = require('csv-parse');
const chai = require('chai');

const utils = require('../server/lib/utils');

const expect = chai.expect;

describe('CSV', () => {
  it('Should transform array to csv string', done => {
    const arr = ['John', 'Doe', 'john.doe@gmail.com'];
    const csv = utils.arrToCSVSting(arr);

    expect(csv).to.equal('John,Doe,john.doe@gmail.com\n');
    done();
  });

  it('Should parse csv file', done => {
    const parser = parse();
    const transformer = utils.transformStream();
    const stream = fs.createReadStream(`${__dirname}/data/data.csv`);

    stream.pipe(parser).pipe(transformer);

    let firstLine;
    let i = 1;
    transformer.on('data', result => {
      if (!firstLine) {
        firstLine = result.record;
        return expect(result.record).to.deep.equal(['name', 'surname', 'email']);
      }
      const data = {
        name: `John-${i}`,
        surname: `Doe-${i}`,
        email: `john.doe-${i}@gmail.com`
      };
      expect(result.record).to.deep.equal([data.name, data.surname, data.email]);
      expect(result.data).to.deep.equal(data);
      i += 1;
    });

    transformer.on('error', done);

    transformer.on('end', done);
  });
});
