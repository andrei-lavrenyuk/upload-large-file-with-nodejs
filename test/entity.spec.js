
const config = require('config');
const mongoose = require('mongoose');
const chai = require('chai');

const Entity = require('../server/models/entity');

const expect = chai.expect;


describe('Entities', () => {
  it('Should save entity', done => {
    mongoose.connect(config.mongo.hosts.join(','), config.mongo.options, () => {
      const data = {
        name: 'John',
        surname: 'Doe',
        email: 'john.doe@gmail.com'
      };
      new Entity(data).save(err => {
        expect(err).to.equal(null);
        done();
      });
    });
    mongoose.connection.on('error', done);
  });

  it('Shouldn\'t save entity', done => {
    mongoose.connect(config.mongo.hosts.join(','), config.mongo.options, () => {
      const data = {
        name: 'John',
        surname: 'Doe'
      };
      new Entity(data).save(err => {
        expect(err).not.to.equal(null);
        done();
      });
    });
    mongoose.connection.on('error', done);
  });
});
